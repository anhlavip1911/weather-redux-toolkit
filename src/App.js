import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import { getWeather, weatherSelector } from './store/reducer/weatherSlice';

const App = () => {
    const weatherGet = useSelector(weatherSelector)
    const dispatch = useDispatch()
    const [query, setQuery] = useState('');
    const search = async (e) => {
        if (e.key === 'Enter') {
            dispatch(getWeather(query));
            setQuery('')
        }
    }

    const dateBuilder = (d) => {
        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        let day = days[d.getDay()];
        let date = d.getDate();
        let month = months[d.getMonth()];
        let year = d.getFullYear();

        return `${day} ${date} ${month} ${year}`
    }

    return (
        <div className="main-container">
            <div className="date">{dateBuilder(new Date())}</div>
            <input type="text" className="search" placeholder="Search..." value={query} onChange={(e) => setQuery(e.target.value)} onKeyPress={search} />
            {(weatherGet.main) ? (
                <div className="city">
                    <h2 className="city-name">
                        <span>{weatherGet.name}</span>
                        <sup>{weatherGet.sys.country}</sup>
                    </h2>
                    <div className="city-temp">
                        {Math.round(weatherGet.main.temp)}
                        <sup>&deg;C</sup>
                    </div>
                    <div className="info">
                        <img className="city-icon" src={`https://openweathermap.org/img/wn/${weatherGet.weather[0].icon}@2x.png`} alt={weatherGet.weather[0].description} />
                        <p>{weatherGet.weather[0].description}</p>
                    </div>
                </div>
            ) : <h1>No data found!</h1>}
        </div>
    );
}

export default App;