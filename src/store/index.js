import {configureStore} from '@reduxjs/toolkit';
import weatherReducer from './reducer/weatherSlice'

const store = configureStore({
    reducer: {
        weatherReducer
    }
})

export default store