import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

const URL = 'https://api.openweathermap.org/data/2.5/weather';
const API_KEY = '83cc7d0a97e92b39bea82205330111fe';

const weatherSlice = createSlice({
  name: 'weather',
  initialState: {
    weatherApi: []
  },
  reducers: {
    weatherFetch(state, action) {
      state.weatherApi = action.payload
    }
  }
})

export const getWeather = (query) => async dispatch => {
  try {
    const response = await axios.get(URL, {
      params: {
        appid: API_KEY,
        units: 'metric',
        q: query
      }
    })
    dispatch(weatherFetch(response.data))
  } catch (error) {
    console.log();
  }
}

// Reducer
const weatherReducer = weatherSlice.reducer

// Selector 
export const weatherSelector = state => state.weatherReducer.weatherApi

// Export action
export const { weatherFetch } = weatherSlice.actions

// Export reducer
export default weatherReducer